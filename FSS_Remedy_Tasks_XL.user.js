// ==UserScript==
// @name         FSS Remedy Tasks XL
// @namespace    https://compass-usa.com
// @version      0.5.3
// @updateURL    https://gist.github.com/WisdomWolf/acb3fdc8555d3c6197ca/raw/FSS_Remedy_Tasks_XL.user.js
// @description  Remove related tasks and enlarge My Tasks table
// @author       WisdomWolf
// @match        https://remedyweb.compass-usa.com/arsys/forms/remedyprod/FSS%3AConsole/Support/*
// @require      https://craig.global.ssl.fastly.net/js/mousetrap/mousetrap.min.js
// @grant        none
// ==/UserScript==
/* jshint -W097 */
'use strict';

function resizeMyTasks() {

    var relTotal = document.getElementById("WIN_0_536870925");
    var relLabel = document.getElementById("WIN_0_1000003644");
    var relTable = document.getElementById("WIN_0_536870915");
    var tskView = document.getElementById("WIN_0_777800485");
    var tskLabel = document.getElementById("WIN_0_536870919");
    var tskTable = document.getElementById("WIN_0_536871015");
    var $welcome = $('.f11.trimJustr').text();

    var tskInnerTable = tskTable.getElementsByClassName("TableInner")[0];
    var tskOuterTable = tskInnerTable.getElementsByClassName("BaseTableOuter")[0];
    var tskBaseTableInner = tskOuterTable.getElementsByClassName("BaseTableInner")[0];

    var createRequest = "https://remedyweb.compass-usa.com/arsys/forms/remedyprod/FSS%3ARequest/Submit/?cacheid=bfc22d50";
    var searchRequest = "https://remedyweb.compass-usa.com/arsys/forms/remedyprod/FSS%3ARequest/Support/?cacheid=2ff4b4fe";
    var searchTask = "https://remedyweb.compass-usa.com/arsys/forms/remedyprod/FSS%3ATask/Support/?cacheid=8ce2378a";

    $("#sub-777802201").children().eq(0).click(function(){ window.open(createRequest, '_blank'); return false;});
    $("#sub-777802201").children().eq(1).click(function(){ window.open(searchRequest, '_blank'); return false;});
    $("#sub-777802201").children().eq(2).click(function(){ window.open(searchTask, '_blank'); return false;});
    $("#sub-777802201").children().eq(3).click(function(){ console.log("denied"); return false;});

    relTotal.style.visibility = "hidden";
    relLabel.style.visibility = "hidden";
    relTable.style.visibility = "hidden";

    tskView.style.top = "251px";
    tskLabel.style.top = "270px"; //View + 19px
    tskTable.style.top = "296px"; //Table = Label + 26
    tskTable.style.height = "285px";
    tskInnerTable.style.height = "258px";
    tskOuterTable.style.height = "258px";
    tskBaseTableInner.style.height = "242px";

    var pattern = new RegExp('Ryan|Amanda');
    if ( pattern.test($welcome) ) {
        window.resizeTo(1200, 780);
    }
}

setTimeout(resizeMyTasks, 500);

Mousetrap.bind('F5', function(e) {
    $('#WIN_0_536871015').find(".Ref.btn.btn3d.TableBtn")[0].click();
    return false;
});
